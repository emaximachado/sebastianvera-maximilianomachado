package principal;

import dominio.*;
import gui.VentanaPrincipal;
import java.io.IOException;

public class Obligatorio {

    public static void main(String[] args) throws IOException {

        Sistema miSistema = new Sistema();
     
        VentanaPrincipal nuevaVentana = new VentanaPrincipal(miSistema);
        nuevaVentana.setVisible(true);

    }

}
