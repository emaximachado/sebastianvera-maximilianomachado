package gui;

import dominio.Sistema;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;

public class VentanaCrearParticipante extends javax.swing.JFrame implements Observer {

    private Sistema sistema;

    public Sistema getSistema() {

        return this.sistema;

    }

    public void setSistema(Sistema unSistema) {

        this.sistema = unSistema;

    }

    public VentanaCrearParticipante(Sistema unSistema) {

        initComponents();

        this.setSistema(unSistema);

        sistema.addObserver(this);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnVolver = new javax.swing.JButton();
        lblIngresarUsuario = new javax.swing.JLabel();
        txtNombreParticipante = new javax.swing.JTextField();
        btnCrearParticipante = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        jPanel1.add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 246, -1));

        lblIngresarUsuario.setForeground(new java.awt.Color(255, 255, 255));
        lblIngresarUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIngresarUsuario.setText("Ingrese el nombre del participante");
        jPanel1.add(lblIngresarUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
        jPanel1.add(txtNombreParticipante, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 246, -1));

        btnCrearParticipante.setText("Crear participante");
        btnCrearParticipante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearParticipanteActionPerformed(evt);
            }
        });
        jPanel1.add(btnCrearParticipante, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 246, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Fondo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 250, 120));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed

        VentanaPrincipal nuevaVentanaPrincipal = new VentanaPrincipal(this.getSistema());

        nuevaVentanaPrincipal.setVisible(true);

        this.dispose();

    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnCrearParticipanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearParticipanteActionPerformed

        if (this.txtNombreParticipante.getText().length() == 0 || this.getSistema().darParticipante(this.txtNombreParticipante.getText()) != null) {

            JOptionPane.showMessageDialog(rootPane, "No puede ingresar un nombre vacío o que ya exista", null, WIDTH, null);

        } else {

            this.getSistema().crearParticipante(this.txtNombreParticipante.getText());

            this.txtNombreParticipante.setText("");

        }
    }//GEN-LAST:event_btnCrearParticipanteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCrearParticipante;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblIngresarUsuario;
    private javax.swing.JTextField txtNombreParticipante;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
