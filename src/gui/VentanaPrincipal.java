package gui;

import dominio.*;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class VentanaPrincipal extends javax.swing.JFrame implements Observer {

    private Sistema sistema;

    public Sistema getSistema() {

        return this.sistema;

    }

    public void setSistema(Sistema unSistema) {

        this.sistema = unSistema;

    }

    public boolean existePencaCreada() {

        boolean existePencaCreada = true;

        if (this.getSistema().getFixture() == null) {

            existePencaCreada = false;

        }

        return existePencaCreada;

    }

    public void actualizarBoxUsuarios() {

        for (int i = 0; i < this.getSistema().getListaParticipantes().size(); i++) {

            this.boxUsuarios.addItem(this.getSistema().getListaParticipantes().get(i).getAlias());

        }

    }

    public void actualizarRanking() {

        this.getSistema().asignarPuntos();

        DefaultTableModel modelo = (DefaultTableModel) this.tblPosiciones.getModel();

        Object[] datos = new Object[2];

        for (int i = 0; i < this.getSistema().getListaParticipantes().size(); i++) {

            datos[0] = this.getSistema().getListaParticipantes().get(i).getAlias();
            datos[1] = this.getSistema().getListaParticipantes().get(i).getPuntos();

            modelo.addRow(datos);

        }

    }

    public VentanaPrincipal(Sistema unSistema) {

        initComponents();

        this.setSistema(unSistema);

        this.getSistema().asignarPuntos();

        this.actualizarRanking();

        sistema.addObserver(this);

        if (this.getSistema().getListaParticipantes().isEmpty()) {

            this.btnJugar.setEnabled(false);

            this.boxUsuarios.setEnabled(false);

        } else {

            this.btnJugar.setEnabled(true);

            this.boxUsuarios.setEnabled(true);

            this.actualizarBoxUsuarios();

        }

        if (!this.existePencaCreada()) {

            this.btnCrearParticipante.setEnabled(false);

        } else {

            this.btnCrearPenca.setEnabled(false);

        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPosiciones = new javax.swing.JTable();
        lblPosiciones = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        btnCrearPenca = new javax.swing.JButton();
        lblSeleccionUsuario = new javax.swing.JLabel();
        boxUsuarios = new javax.swing.JComboBox<>();
        btnJugar = new javax.swing.JButton();
        btnCrearParticipante = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblPosiciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Participante", "Puntos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblPosiciones);
        if (tblPosiciones.getColumnModel().getColumnCount() > 0) {
            tblPosiciones.getColumnModel().getColumn(0).setResizable(false);
            tblPosiciones.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 325, -1));

        lblPosiciones.setForeground(new java.awt.Color(255, 255, 255));
        lblPosiciones.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosiciones.setText("Tabla de posiciones");
        jPanel1.add(lblPosiciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 325, -1));

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 420, 256, -1));

        btnCrearPenca.setText("Crear penca");
        btnCrearPenca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearPencaActionPerformed(evt);
            }
        });
        jPanel1.add(btnCrearPenca, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 30, 256, -1));

        lblSeleccionUsuario.setForeground(new java.awt.Color(255, 255, 255));
        lblSeleccionUsuario.setText("Seleccione el usuario que va a jugar");
        jPanel1.add(lblSeleccionUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 100, -1, -1));

        jPanel1.add(boxUsuarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 120, 256, -1));

        btnJugar.setText("Jugar");
        btnJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJugarActionPerformed(evt);
            }
        });
        jPanel1.add(btnJugar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 150, 256, -1));

        btnCrearParticipante.setText("Crear participante");
        btnCrearParticipante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearParticipanteActionPerformed(evt);
            }
        });
        jPanel1.add(btnCrearParticipante, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 60, 256, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Fondo.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 600, 460));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCrearPencaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearPencaActionPerformed

        VentanaCrearPenca nuevaVentanaCrearPenca = new VentanaCrearPenca(this.getSistema());

        nuevaVentanaCrearPenca.setVisible(true);

        this.dispose();

    }//GEN-LAST:event_btnCrearPencaActionPerformed

    private void btnJugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJugarActionPerformed

        VentanaJugar nuevaVentanaJugar = new VentanaJugar(this.getSistema(), this.getSistema().darParticipante(this.boxUsuarios.getSelectedItem().toString()));

        nuevaVentanaJugar.setVisible(true);

        this.dispose();

    }//GEN-LAST:event_btnJugarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        this.dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCrearParticipanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearParticipanteActionPerformed

        VentanaCrearParticipante nuevaVentanaCrearParticipante = new VentanaCrearParticipante(this.getSistema());

        nuevaVentanaCrearParticipante.setVisible(true);

        this.dispose();

    }//GEN-LAST:event_btnCrearParticipanteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> boxUsuarios;
    private javax.swing.JButton btnCrearParticipante;
    private javax.swing.JButton btnCrearPenca;
    private javax.swing.JButton btnJugar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblPosiciones;
    private javax.swing.JLabel lblSeleccionUsuario;
    private javax.swing.JTable tblPosiciones;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
