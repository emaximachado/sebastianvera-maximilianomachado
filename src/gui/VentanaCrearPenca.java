package gui;

import dominio.*;
import static java.awt.image.ImageObserver.WIDTH;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class VentanaCrearPenca extends javax.swing.JFrame implements Observer {

    private Sistema sistema;

    public Sistema getSistema() {

        return this.sistema;

    }

    public void setSistema(Sistema unSistema) {

        this.sistema = unSistema;

    }

    public void cerrarVentana() {

        VentanaPrincipal nuevaVentanaPrincipal = new VentanaPrincipal(this.getSistema());

        nuevaVentanaPrincipal.setVisible(true);

        this.dispose();

    }

    public VentanaCrearPenca(Sistema unSistema) {

        initComponents();

        this.setSistema(unSistema);

        sistema.addObserver(this);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        btnVolver = new javax.swing.JButton();
        boxMultiplicadorCuartos = new javax.swing.JComboBox<>();
        btnCrearPenca = new javax.swing.JButton();
        lblMultiplicador5 = new javax.swing.JLabel();
        lblPuntosPorGoles = new javax.swing.JLabel();
        boxMultiplicadorSemifinal = new javax.swing.JComboBox<>();
        boxPuntosPorGoles = new javax.swing.JComboBox<>();
        lblMultiplicador6 = new javax.swing.JLabel();
        lblPuntosPorGanador = new javax.swing.JLabel();
        boxMultiplicadorFinal = new javax.swing.JComboBox<>();
        boxPuntosPorGanador = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        lblMultiplicador1 = new javax.swing.JLabel();
        boxPuntosPorEmpate = new javax.swing.JComboBox<>();
        boxMultiplicadorGrupos = new javax.swing.JComboBox<>();
        lblMultiplicador3 = new javax.swing.JLabel();
        boxMultiplicadorOctavos = new javax.swing.JComboBox<>();
        lblMultiplicador4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnVolver.setText("Volver");
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });
        jPanel2.add(btnVolver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 420, -1, -1));

        boxMultiplicadorCuartos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxMultiplicadorCuartos.setSelectedItem(null);
        jPanel2.add(boxMultiplicadorCuartos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 306, -1));

        btnCrearPenca.setText("Crear penca");
        btnCrearPenca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearPencaActionPerformed(evt);
            }
        });
        jPanel2.add(btnCrearPenca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 420, -1, -1));

        lblMultiplicador5.setForeground(new java.awt.Color(255, 255, 255));
        lblMultiplicador5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplicador5.setText("Multiplicador de puntaje en la semifinal");
        jPanel2.add(lblMultiplicador5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 306, -1));

        lblPuntosPorGoles.setForeground(new java.awt.Color(255, 255, 255));
        lblPuntosPorGoles.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPuntosPorGoles.setText("Puntos por acertar los goles");
        jPanel2.add(lblPuntosPorGoles, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 306, -1));

        boxMultiplicadorSemifinal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxMultiplicadorSemifinal.setSelectedItem(null);
        jPanel2.add(boxMultiplicadorSemifinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 306, -1));

        boxPuntosPorGoles.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxPuntosPorGoles.setSelectedItem(null);
        jPanel2.add(boxPuntosPorGoles, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 306, -1));

        lblMultiplicador6.setForeground(new java.awt.Color(255, 255, 255));
        lblMultiplicador6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplicador6.setText("Multiplicador de puntaje en la final");
        jPanel2.add(lblMultiplicador6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 370, 306, -1));

        lblPuntosPorGanador.setForeground(new java.awt.Color(255, 255, 255));
        lblPuntosPorGanador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPuntosPorGanador.setText("Puntor por acertar ganador");
        jPanel2.add(lblPuntosPorGanador, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 306, -1));

        boxMultiplicadorFinal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxMultiplicadorFinal.setSelectedItem(null);
        jPanel2.add(boxMultiplicadorFinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 390, 306, -1));

        boxPuntosPorGanador.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxPuntosPorGanador.setSelectedItem(null);
        jPanel2.add(boxPuntosPorGanador, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 306, -1));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Puntos por acertar el empate");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 306, -1));

        lblMultiplicador1.setForeground(new java.awt.Color(255, 255, 255));
        lblMultiplicador1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplicador1.setText("Multiplicador de puntaje en fase de grupos");
        jPanel2.add(lblMultiplicador1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, -1, -1));

        boxPuntosPorEmpate.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxPuntosPorEmpate.setSelectedItem(null);
        jPanel2.add(boxPuntosPorEmpate, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 130, 311, -1));

        boxMultiplicadorGrupos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxMultiplicadorGrupos.setSelectedItem(null);
        jPanel2.add(boxMultiplicadorGrupos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 306, -1));

        lblMultiplicador3.setForeground(new java.awt.Color(255, 255, 255));
        lblMultiplicador3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplicador3.setText("Multiplicador de puntaje en octavos de final");
        jPanel2.add(lblMultiplicador3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 210, -1, -1));

        boxMultiplicadorOctavos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        boxMultiplicadorOctavos.setSelectedItem(null);
        jPanel2.add(boxMultiplicadorOctavos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 306, -1));

        lblMultiplicador4.setForeground(new java.awt.Color(255, 255, 255));
        lblMultiplicador4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMultiplicador4.setText("Multiplicador de puntaje en caurtos de final");
        jPanel2.add(lblMultiplicador4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 270, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Fondo.jpg"))); // NOI18N
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 310, 447));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed

        this.cerrarVentana();

    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnCrearPencaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearPencaActionPerformed

        if (this.boxPuntosPorGoles.getSelectedItem() == null || this.boxPuntosPorGanador.getSelectedItem() == null || this.boxPuntosPorEmpate.getSelectedItem() == null || this.boxMultiplicadorGrupos.getSelectedItem() == null || this.boxMultiplicadorOctavos.getSelectedItem() == null || this.boxMultiplicadorCuartos.getSelectedItem() == null || this.boxMultiplicadorSemifinal.getSelectedItem() == null || this.boxMultiplicadorFinal.getSelectedItem() == null) {

            JOptionPane.showMessageDialog(rootPane, "Debe completar todos los campos solicitados. \nIngresar los puntos por acertar el ganador, los goles o el empate, \nasí como los multiplicadores de puntos en las instancias finales", null, WIDTH, null);

        } else {

            int[] multiplicadores = new int[5];

            multiplicadores[0] = Integer.parseInt(this.boxMultiplicadorGrupos.getSelectedItem().toString());
            multiplicadores[1] = Integer.parseInt(this.boxMultiplicadorOctavos.getSelectedItem().toString());
            multiplicadores[2] = Integer.parseInt(this.boxMultiplicadorCuartos.getSelectedItem().toString());
            multiplicadores[3] = Integer.parseInt(this.boxMultiplicadorSemifinal.getSelectedItem().toString());
            multiplicadores[4] = Integer.parseInt(this.boxMultiplicadorFinal.getSelectedItem().toString());

            this.getSistema().crearReglas(multiplicadores, Integer.parseInt(this.boxPuntosPorGoles.getSelectedItem().toString()), Integer.parseInt(this.boxPuntosPorGanador.getSelectedItem().toString()), Integer.parseInt(this.boxPuntosPorEmpate.getSelectedItem().toString()));

            try {
                this.getSistema().crearFixture("Partidos.txt");
                this.getSistema().cargarGrupos("Grupos.txt");
                this.getSistema().cargarResultados("Resultados.txt");
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(rootPane, "No se pudo acceder al archivo,\n verifica que el archivo existe \n y esta al lado del ejecutable", null, WIDTH, null);
                this.dispose();
            } catch (IOException ex) {
                Logger.getLogger(VentanaCrearPenca.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            this.cerrarVentana();

        }

    }//GEN-LAST:event_btnCrearPencaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> boxMultiplicadorCuartos;
    private javax.swing.JComboBox<String> boxMultiplicadorFinal;
    private javax.swing.JComboBox<String> boxMultiplicadorGrupos;
    private javax.swing.JComboBox<String> boxMultiplicadorOctavos;
    private javax.swing.JComboBox<String> boxMultiplicadorSemifinal;
    private javax.swing.JComboBox<String> boxPuntosPorEmpate;
    private javax.swing.JComboBox<String> boxPuntosPorGanador;
    private javax.swing.JComboBox<String> boxPuntosPorGoles;
    private javax.swing.JButton btnCrearPenca;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblMultiplicador1;
    private javax.swing.JLabel lblMultiplicador3;
    private javax.swing.JLabel lblMultiplicador4;
    private javax.swing.JLabel lblMultiplicador5;
    private javax.swing.JLabel lblMultiplicador6;
    private javax.swing.JLabel lblPuntosPorGanador;
    private javax.swing.JLabel lblPuntosPorGoles;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
