package utilidades;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ArchivoLectura {

    private String linea = "";
    private BufferedReader in;

    public ArchivoLectura(String unNombre) {
        try {
            in = new BufferedReader(new FileReader(unNombre));
        } catch (FileNotFoundException e) {
            System.out.println("El archivo no existe");
        }

    }

    public boolean hayMasLineas() {
        try {
            linea = in.readLine();
        } catch (IOException e) {
            linea = null;
        }
        return linea != null;
    }

    public String linea() {
        return linea;
    }

    public boolean cerrar() {

        try {
            in.close();
            return true;

        } catch (IOException e) {
            System.out.println("Error al cerrar archivo");
            return false;
        }

    }

}
