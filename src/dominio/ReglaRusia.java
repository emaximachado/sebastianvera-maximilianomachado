package dominio;

import interfaces.Reglas;

/**
 *
 * @author Sebas
 */
public class ReglaRusia implements Reglas {
    
    private int[] fases;
    private int goles;
    private int ganador;
    private int empate;

    
    public ReglaRusia(int[] lasFases, int losGoles, int elGanador, int esEmpate) {
        fases = lasFases;
        goles = losGoles;
        ganador = elGanador;
        empate = esEmpate;
    }
    
    @Override
    public int darPuntaje(PartidoRusia apuesta, FixtureRusia fixture) {
        double retorno = 0;
        PartidoRusia partidoEnFixture;
        
        if (apuesta != null) {
            int numeroPartido = apuesta.getNumeroPartido();
            
            if (fixture.esPosicionVacia(numeroPartido)) {
                throw new NullPointerException("Esta intentado acceder a una posicion nula");
                
            } else {
                partidoEnFixture = fixture.darPartido(numeroPartido);    
                
                if (partidoEnFixture.isEmpate() && apuesta.isEmpate()) {
                    retorno += empate;
                    
                } else if (partidoEnFixture.getGanador().equals(apuesta.getGanador())) {
                    retorno += ganador;
                }
                if (partidoEnFixture.getResultado()[0] == apuesta.getResultado()[0]) {
                    retorno += (double) goles / 2;
                }
                if (partidoEnFixture.getResultado()[1] == apuesta.getResultado()[1]) {
                    retorno += (double) goles / 2;
                }
                if (partidoEnFixture.getNumeroPartido() <= 48) {
                    retorno *= fases[0];
                } else if (partidoEnFixture.getNumeroPartido() <= 56) {
                    retorno *= fases[1];
                } else if (partidoEnFixture.getNumeroPartido() <= 60) {
                    retorno *= fases[2];
                } else if (partidoEnFixture.getNumeroPartido() <= 63) {
                    retorno *= fases[3];
                } else {
                    retorno *= fases[4];
                }
            }    

           
        }
        return (int)Math.round(retorno);
    }
    
}
