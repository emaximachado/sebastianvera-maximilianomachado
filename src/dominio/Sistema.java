package dominio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Observable;
import utilidades.ArchivoLectura;

/**
 * La clase sistema contiene los atributos necesarios para la implementacion
 * logica de la solucion. Algunos de sus metodos desarrollan algunos de los
 * casos de uso
 * <p>
 * Sistema contiene un atributo ultimaActualizacion que guarda la fecha de la
 * ultima actualizacion, una lista de participantes, un fixture y sus reglas.
 *
 * @author Sebas
 */
public class Sistema extends Observable {

    private LocalDateTime ultimaActualizacion;
    private ArrayList<ParticipanteRusia> listaParticipantes;
    private FixtureRusia fixture;
    private ReglaRusia reglas;

    /**
     * Constructor sin parametros. <p>
     * Se instancia con fecha de actualizacion 1-5-2018 de modo que el
     * sistema pueda actualizar los resultados al ser
     * iniciado en cualquier momento durante el mundial
     * <p>
     * Nota:
     * Para poder utilizar Sistema es necesario que se ejecuten los siguientes metodos:
     * <p>
     *
     * crearReglas(): crearFixture(); cargarGrupos(); crearParticipante();
     *
     * @param fecha
     */
    public Sistema() {
        ultimaActualizacion = LocalDateTime.of(2018, 5, 1, 0, 0);
        listaParticipantes = new ArrayList<>();
    }

    
    public ArrayList<ParticipanteRusia> getListaParticipantes () {
        return listaParticipantes;
    }

    /**
     * crearReglas es un metodo que permite definir las reglas por las cuales
     * los participantes recibiran puntos
     *
     * @see Reglas
     * @param fases recibe un array de enteros de tamano 5 con los modificadores
     * para cada fase
     * @param goles recibe un int con el modificador para los goles
     * @param ganador recibe un int para el modificador del ganador
     * @param empate recibe un int para el modificador de empate
     * @return boolean de confirmacion
     */
    public boolean crearReglas(int[] fases, int goles, int ganador, int empate) {
        ReglaRusia regla = new ReglaRusia(fases, goles, ganador, empate);
        reglas = regla;
        return true;
    }

    /**
     * crearFixture es un metodo que permite crear una instancia de la clase
     * fixtureRusia, y establecerla como atributo de sistema.
     *
     * @param fixture recibe un archivo txt con los datos del fixture
     * @return boolean de confirmacion
     */
    public boolean cargarGrupos(String fileGrupos) throws FileNotFoundException, IOException {
        ArchivoLectura al = new ArchivoLectura(fileGrupos);
        PartidoRusia[] partidos = new PartidoRusia[65];
        SeleccionRusia[][] grupos = new SeleccionRusia[8][4];
        int numeroGrupo = 0;
        

        while (al.hayMasLineas()) {

            String[] campos = al.linea().split("#");
            SeleccionRusia equipo1 = new SeleccionRusia(campos[0]);
            SeleccionRusia equipo2 = new SeleccionRusia(campos[1]);
            SeleccionRusia equipo3 = new SeleccionRusia(campos[2]);
            SeleccionRusia equipo4 = new SeleccionRusia(campos[3]);

            grupos[numeroGrupo][0] = equipo1;
            grupos[numeroGrupo][1] = equipo2;
            grupos[numeroGrupo][2] = equipo3;
            grupos[numeroGrupo][3] = equipo4;
            numeroGrupo++;
        }
        al.cerrar();
        fixture.setFaseGrupos(grupos);
        return true;
    }

    public boolean crearFixture(String fileFixture) throws FileNotFoundException, IOException {
        boolean ok = false;
        ArchivoLectura al = new ArchivoLectura(fileFixture);
        PartidoRusia[] partidos = new PartidoRusia[65];

        while (al.hayMasLineas()) {

            String[] campos = al.linea().split("#");

            int numeroPartido = Integer.parseInt(campos[0]);
            String seleccionA = campos[4];
            String seleccionB = campos[5];
            String estadio = campos[7];
            String grupo = campos[6];
            int dia = Integer.parseInt(campos[1]);
            int mes = Integer.parseInt(campos[2]);
            int hora = Integer.parseInt(campos[3]);
            
            LocalDateTime fecha = LocalDateTime.of(2018, mes, dia, hora, 0);

            PartidoRusia unPartido;
            unPartido = new PartidoRusia(numeroPartido, seleccionA, seleccionB, estadio);
            unPartido.setGrupo(grupo);
            unPartido.setFechaYHora(fecha);
            partidos[numeroPartido] = unPartido;
        }

        FixtureRusia elFixture = new FixtureRusia(partidos);

        fixture = elFixture;
        ok = true;

        al.cerrar();
        return ok;
    }

    /**
     * crearParticipante es un metodo que permite crear una instancia de la
     * clase participanteRusia y agregarlo como atributo de sistma.
     * ParticipanteRusia tiene como atributo un fixture, el mismo es una copia
     * del fixture de sistema, solo que no debe compartir memoria, de modo que
     * las modificaciones en el fixture de participante no se veran reflejadas
     * en el fixture de sistemas
     *
     * @param alias recibe un string con el alais del jugador, el alias no ha
     * sido ingresado aun.
     * @return boolean de confirmacion
     */
    public boolean crearParticipante(String alias) {

        FixtureRusia fixtureParticipante = fixture.clon();
        
        ParticipanteRusia p1 = new ParticipanteRusia(alias, fixtureParticipante);
        return listaParticipantes.add(p1);
    }

    /**
     * cargarResultados es un metodo que en caso de que el sistema no este
     * actualizado recibe un archivo de texto con los resultados en el. El
     * fixture se actualiza segun los datos que contenga el fixture.
     *
     * @param resultados recibe un archivo txt con los datos de los partidos.
     * los mismos se cargan hasta la fecha actual del sistema.
     * @return boolean de confirmacion
     */

    public boolean cargarResultados(String fileResultados) throws IOException {

        int numeroPartidoProximo = 1;
        int numeroPartidoHasta = fixture.darProximoPartido(LocalDateTime.now());

        ArchivoLectura al = new ArchivoLectura(fileResultados);
        ArrayList<Integer> listaResultados = new ArrayList<>();

        while (al.hayMasLineas() && numeroPartidoProximo < numeroPartidoHasta) {

            String[] campos = al.linea().split("#");

            int numeroPartido = Integer.parseInt(campos[0]);
            int resultado1 = Integer.parseInt(campos[1]);
            int resultado2 = Integer.parseInt(campos[2]);
            listaResultados.add(numeroPartido);
            listaResultados.add(resultado1);
            listaResultados.add(resultado2);

            numeroPartidoProximo = numeroPartido + 1;

        }

        fixture.cargarResultados(listaResultados);

        al.cerrar();
        this.setUltimaActualizacion(LocalDateTime.now());
        return true;

    }

    public ParticipanteRusia darParticipante(String alias) {
        boolean encontrado = false;
        ParticipanteRusia participante = null;
        
        for (int i = 0; i < this.getListaParticipantes().size() && !encontrado; i++) {
            if (this.getListaParticipantes().get(i).getAlias().equals(alias)) {
                encontrado = true;
                participante = this.getListaParticipantes().get(i);
            }
        }
        return participante;
    }

    /**
     * asignarPuntos es un metodo que asigna los puntos a los participantes<p>
     * recorre toda la lista de participantes, levanta cada apuesta de cada
     * participante y usa la instancia reglas para resolver los puntos del
     * participante y luego los asigna al participante.
     * <p>
     * Las apuestas sobre partidos que aun no se han jugado no deberan ser
     * tenidas en cuenta y deberan permanecer en la lista de apuestas del
     * participante. Las apuestas sobre partidos terminados deberan ser
     * eliminadas al terminarce de otorgar los puntos.
     *
     * @return boolean de confirmacion
     */
    public boolean asignarPuntos() {
        ParticipanteRusia unParticipante;
        PartidoRusia apuesta;
        int puntos = 0;
        int numeroPartido;
        ArrayList<Integer> listaNumerosPartido = new ArrayList<>();

    /*
    Recorre la lista de participantes, por cada uno recorre la lista de apuestas
    Por cada apuesta toma el numero de partido y consulta a fixture si termino
    si termino obtiene los puntos los acumula y enlista el numero de partido para
    luego eliminar esas apuestas de la lista de apuestas del participante
    */ 
        for (int i = 0; i < listaParticipantes.size(); i++) {
            unParticipante = listaParticipantes.get(i);
            
            for (int j = 0; j < unParticipante.darApuestas().size(); j++) {
                apuesta = unParticipante.darApuestas().get(j);
                numeroPartido = apuesta.getNumeroPartido();

                if (fixture.darPartido(numeroPartido).isTermino()) {
                    puntos += reglas.darPuntaje(apuesta, fixture);
                    listaNumerosPartido.add(apuesta.getNumeroPartido());
                }
            }
            unParticipante.agregarPuntos(puntos);
            unParticipante.vaciarApuestasParcial(listaNumerosPartido);
        }

        return true;
    }

    /**
     * estaActualizado es un metodo que dice si el sistema esta actualizado o
     * no. Utiliza la ultimaActualizacion, la fecha actual del sistema y hace un
     * pedido al fixture con cada fecha
     *
     * @return retorna true si los numeros de partidos son iguales, false si son
     * distintos
     */

    public boolean estaActualizado() {
        LocalDateTime fechaActual = LocalDateTime.now();

        int partidoUltimaActualizacion = fixture.darProximoPartido(this.getUltimaActualizacion());
        int partidoFechaActual = fixture.darProximoPartido(fechaActual);

        return partidoFechaActual == partidoUltimaActualizacion;

    }
    
    public LocalDateTime getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(LocalDateTime ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public FixtureRusia getFixture() {
        return fixture;
    }

}
