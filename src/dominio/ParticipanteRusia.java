package dominio;

import interfaces.Participante;
import java.util.ArrayList;


public class ParticipanteRusia implements Participante {

    private String alias;
    private int puntos;
    private FixtureRusia apuestas;

    
    /**
     * Constructor de la clase
     *
     * @param elAlias
     * @param elFixture
     */
    public ParticipanteRusia(String elAlias, FixtureRusia elFixture) {
        alias = elAlias;
        puntos = 0;
        apuestas = elFixture;
    }

    /**
     * Métodos get y set para acceder a los atributos de la clase
     *
     * @return
     */
    public String getAlias() {
        return this.alias;
    }

    public int getPuntos() {
        return this.puntos;
    }

    
    public FixtureRusia getFixture() {
        return apuestas;
    }

    /**
     * Implementación del método agregarApuesta descripto en la interfaz
     * Participante
     *
     * @param unPartido
     * @return
     */
    @Override
    public boolean agregarApuesta(PartidoRusia unPartido) {
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(unPartido.getNumeroPartido());
        listaResultado.add(unPartido.getResultado()[0]);
        listaResultado.add(unPartido.getResultado()[1]);
        
        this.getFixture().cargarResultados(listaResultado);

        return true;
    }

    /**
     * Implementación del método darApuestas descripto en la interfaz
     * Participante
     *
     * @return
     */
    @Override
    public ArrayList<PartidoRusia> darApuestas() {
        ArrayList<PartidoRusia> listaRetorno = new ArrayList<>();
        for (int i = 0; i < apuestas.getFixture().length; i++) {
            if (apuestas.getFixture()[i] != null && apuestas.getFixture()[i].isTermino()) {
                listaRetorno.add(apuestas.getFixture()[i]);
            }
        }
        return listaRetorno;
    }

    /**
     * Implementación del método vaciarApuestasParcial descripto en la interfaz
     * Participante. Se guardan en un array de int los números de partidos a
     * borrar, luego, se recorre ese array y se compara si el número de partido
     * corresponde con un número de partido guardado en la lista de apuestas que
     * tiene el participante, en caso de que sean iguales, se borra ese objeto
     * de la lista
     *
     * @param listaPartidos
     * @return
     */
    @Override
    public boolean vaciarApuestasParcial(ArrayList<Integer> listaPartidos) {
        boolean eliminados = false;
        for (int i = 0; i < listaPartidos.size(); i++) {
            this.apuestas.boorrarPartido(listaPartidos.get(i));
        }
        return eliminados;
    }

    /**
     * Implementación del método agregarPuntos descripto en la interfaz
     * Participante
     *
     * @param misPuntos
     * @return
     */
    @Override
    public boolean agregarPuntos(int misPuntos) {
        boolean agregados = true;
        puntos = puntos + misPuntos;
        return agregados;
    }

}
