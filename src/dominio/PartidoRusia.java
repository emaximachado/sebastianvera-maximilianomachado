package dominio;

import java.time.LocalDateTime;

/**
 * PartidoRusia es la clase que guarda los datos de los partidos y por tanto es
 * la clase eje en el funcionamiento de la aplicacion. Consta de los atributos
 * necesarios para su funcionamiento asi como tambien informacion que pueda ser
 * mostrada en la interfaz grafica.
 *
 * @author Sebas
 */
public class PartidoRusia {

    private int numeroPartido;
    private String seleccionA;
    private String seleccionB;
    private int[] resultado;
    private String estadio;
    private LocalDateTime fechaYHora;
    private boolean termino = false;
    private String ganador;
    private boolean empate;
    private String grupo;

    /**
     * Constructor con parametros reducido. Luego de instanciar deberian
     * agregarse los siguientes atributos atraves de los metodos de acceso y
     * modificacion.
     * <p>
     * setResultado<p>
     * setFechaYHora<p>
     * setGrupo<p>
     * Si corresponde (porque sea un partido terminado)
     * <p>
     * setTermino<p>
     * setGanador<p>
     * setEmpate(es un booleano que debe ser definido si o si)
     * <p>
     *
     * @param elNumero int con el numero de partido
     * @param equipoA string con el equipo A
     * @param equipoB string con el equipo B
     * @param elEstadio string con el nombre del estadio
     */
    public PartidoRusia(int elNumero, String equipoA, String equipoB, String elEstadio) {
        numeroPartido = elNumero;
        seleccionA = equipoA;
        seleccionB = equipoB;
        estadio = elEstadio;
    }

    public String getGrupo() {
        return grupo;
    }


    /**
     * Constructor para pruebas recibe aparte el parametro fin que permite
     * establecerlo como terminado, asi como tambien establecer ganador o empate
     *
     * @param elNumero
     * @param equipoA
     * @param equipoB
     * @param res
     * @param elEstadio
     * @param laFecha
     * @param fin
     */
    public PartidoRusia(int elNumero, String equipoA, String equipoB,
            int[] res, String elEstadio, LocalDateTime laFecha, boolean fin,
            String elGanador, boolean esEmpate) {

        numeroPartido = elNumero;
        seleccionA = equipoA;
        seleccionB = equipoB;
        resultado = res;
        estadio = elEstadio;
        fechaYHora = laFecha;
        termino = fin;
        ganador = elGanador;
        empate = esEmpate;
    }

        /**
         * Constructor copia el cual no comparte memoria con el parametro
         * recibido
         *
         * @param elPartido recibe un partido no vacio
         */
    public PartidoRusia(PartidoRusia elPartido) {

        numeroPartido = elPartido.numeroPartido;
        seleccionA = new String(elPartido.seleccionA);
        seleccionB = new String(elPartido.seleccionB);
        estadio = new String(elPartido.estadio);
        grupo = new String(elPartido.grupo);
    }

    public void setFechaYHora(LocalDateTime fechaYHora) {
        this.fechaYHora = fechaYHora;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    public void setEmpate(boolean empate) {
        this.empate = empate;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setResultado(int[] resultado) {
        this.resultado = resultado;
    }

    public void setTermino() {
        this.termino = true;
    }

    public int getNumeroPartido() {
        return numeroPartido;
    }

    public String getSeleccionA() {
        return seleccionA;
    }

    public String getSeleccionB() {
        return seleccionB;
    }

    public int[] getResultado() {
        return resultado;
    }

    public boolean isTermino() {
        return termino;
    }

    public boolean isEmpate() {
        return empate;
    }

    public String getEstadio() {
        return estadio;
    }

    public LocalDateTime getFechaYHora() {
        return fechaYHora;
    }

    public String getGanador() {
        return ganador;
    }

    @Override
    public boolean equals(Object o) {

        return this.getEstadio().equals(((PartidoRusia) o).getEstadio())
                && this.getFechaYHora().isEqual(((PartidoRusia) o).getFechaYHora())
                && this.getGanador().equals(((PartidoRusia) o).getGanador())
                && this.getGrupo().equals(((PartidoRusia) o).getGrupo())
                && this.isEmpate() == ((PartidoRusia) o).isEmpate()
                && this.getNumeroPartido() == ((PartidoRusia) o).getNumeroPartido()
                && this.getResultado()[0] == ((PartidoRusia) o).getResultado()[0]
                && this.getResultado()[1] == ((PartidoRusia) o).getResultado()[1]
                && this.getSeleccionA().equals(((PartidoRusia) o).getSeleccionA())
                && this.getSeleccionB().equals(((PartidoRusia) o).getSeleccionB());
    
    }

}
