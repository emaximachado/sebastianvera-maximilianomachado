package dominio;

import interfaces.Fixture;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * FixtureRusia implementa la interfaz fixture y define un aributo fixture como
 * un ArrayList de partidos ordenado por el numero de partidos. Cada numero de
 * partido corresponde al mismo indice del Array. A su ves el atributo grupos es
 * una matriz que contiene por cada fila un grupo del mundial.
 *
 * @author Sebas
 */
public class FixtureRusia implements Fixture {

    private PartidoRusia[] fixture;
    private SeleccionRusia[][] faseGrupos;

    /**
     * Constructor con parametros, recibe un arraylist de partidos ordenados por
     * numero de partdos
     *
     * @param listaPartidos Constructor con parametros, recibe un arraylist de
     * partidos ordenados por numero de partdos
     * @param listaPartidos
     */
    public FixtureRusia(PartidoRusia[] listaPartidos) {
        fixture = listaPartidos;
        faseGrupos = new SeleccionRusia[8][4];
    }

    public void setFaseGrupos(SeleccionRusia[][] faseGrupos) {
        this.faseGrupos = faseGrupos;
    }

    @Override
    public PartidoRusia darPartido(int noPartido) {
        return fixture[noPartido];
    }

    public void boorrarPartido(int noPartido) {
        fixture[noPartido] = null;
    }

    public PartidoRusia[] getFixture() {
        return fixture;
    }

    @Override
    public int darProximoPartido(LocalDateTime fecha) {
        PartidoRusia partido;
        int numeroPartido = 0;
        boolean ok = false;
        for (int i = 1; i < fixture.length && !ok; i++) {
            partido = fixture[i];
            if (partido.getFechaYHora().isAfter(fecha)) {
                numeroPartido = partido.getNumeroPartido();
                ok = true;
            }
        }
        return numeroPartido;
    }

    @Override
    public boolean esPosicionVacia(int index) {
        return fixture[index] == null;
    }

    @Override
    public SeleccionRusia[][] darGrupos() {
        return faseGrupos;
    }

    @Override

    public boolean cargarResultados(ArrayList<Integer> listaResultado) {

        for (int i = 0; i < listaResultado.size(); i = i + 3) {
            int numeroPartido = listaResultado.get(i);
            int golesA = listaResultado.get(i + 1);
            int golesB = listaResultado.get(i + 2);

            PartidoRusia partido = fixture[numeroPartido];
            String grupo = partido.getGrupo();

            if (golesA == golesB) {
                partido.setEmpate(true);
                partido.setGanador("");

                for (int j = 0; j < faseGrupos[grupo.compareTo("A")].length; j++) {
                    if (faseGrupos[grupo.compareTo("A")][j].getSeleccion().equals(partido.getSeleccionA())) {
                        faseGrupos[grupo.compareTo("A")][j].setPuntos(1);
                    }
                }
                for (int j = 0; j < faseGrupos[grupo.compareTo("A")].length; j++) {
                    if (faseGrupos[grupo.compareTo("A")][j].getSeleccion().equals(partido.getSeleccionB())) {
                        faseGrupos[grupo.compareTo("A")][j].setPuntos(1);
                    }
                }
            }
            if (golesA > golesB) {

                partido.setGanador(partido.getSeleccionA());
                partido.setEmpate(false);

                for (int j = 0; j < faseGrupos[grupo.compareTo("A")].length; j++) {
                    if (faseGrupos[grupo.compareTo("A")][j].getSeleccion().equals(partido.getSeleccionA())) {
                        faseGrupos[grupo.compareTo("A")][j].setPuntos(3);
                    }
                }
            }
            if (golesB > golesA) {

                partido.setGanador(partido.getSeleccionB());
                partido.setEmpate(false);
                for (int j = 0; j < faseGrupos[grupo.compareTo("A")].length; j++) {
                    if (faseGrupos[grupo.compareTo("A")][j].getSeleccion().equals(partido.getSeleccionB())) {
                        faseGrupos[grupo.compareTo("A")][j].setPuntos(3);
                    }
                }
            }
            int[] resultado = {golesA, golesB};
            partido.setTermino();
            partido.setResultado(resultado);
            Arrays.sort(faseGrupos[grupo.compareTo("A")], (a, b)-> b.getPuntos() - a.getPuntos());
        }
        return true;
    }

    FixtureRusia clon() {
        PartidoRusia[] listaPartidos = new PartidoRusia[65];
        for (int i = 1; i < listaPartidos.length; i++) {
            listaPartidos[i] = new PartidoRusia(fixture[i]);
        }
        SeleccionRusia[][] grupos = new SeleccionRusia[8][4];
        for (int i = 0; i < grupos.length; i++) {
            for (int j = 0; j < grupos[i].length; j++) {
                grupos[i][j] = faseGrupos[i][j].clon();
            }
        }

        FixtureRusia fixtureRetorno = new FixtureRusia(listaPartidos);
        fixtureRetorno.setFaseGrupos(grupos);
        return fixtureRetorno;
    }
}
