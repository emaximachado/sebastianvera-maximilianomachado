package interfaces;

import dominio.PartidoRusia;
import java.util.ArrayList;
/**
 * Participante es una interfaz que describe el comportamiento que tiene un 
 * participante en el sistema. Almacena como atributo un FixtureRusia
 * el cual seran considerado como apuestas. En las descripciones de los metodos refiere
 * a apuestas como la lista de partidos del Fixture.
 * @author Sebas
 */

public interface Participante {
    
    /**
     * AgregarApuesta es un metodo que permite recibir un partido y lo guarda
     * como atributo. El mismo al ser una apuesta es un partido que aun
     * no se a jugado por tanto oficia como pronostico. 
     * 
     * @param PartidoRusia recibe un partido con sus atributos ingresados
     * correctamente.
     * @return boolean de confirmacion
     */
    boolean agregarApuesta(PartidoRusia partido);
    
    /**
     * darApuestas es un metodo que permite mostrar las apuestas actuales de
     * un participante
     * @return retorna una copia de la lista actual de apuestas de modo que 
     * el atributo no pueda ser modificado posteriormente.
     */
    ArrayList<PartidoRusia> darApuestas();
    
    /**
     * vaciarApuestasParcial es un metodo que permite vaciar el contenido de la
     * lista de forma parcial o total una ves que alla sido utilizada para 
     * otorgar los puntos al jugador
     * 
     * @param  listaPartidos recibe una lista ordenada de enteros que refieren a
     * los numeros de partidos correspondiente que se deben eliminar de la
     * lista de apuestas. 
     * @return boolean de confirmacion
     */
    boolean vaciarApuestasParcial(ArrayList<Integer> listaPartidos);   
    
    /**
     * agregarPUntos es un metodo que permite recibir un entero que representa
     * los puntos que el participante recibe.
     * @param misPuntos recibe un entero mayor o igual a 0
     * @return boolean de confiramcion
     */
    boolean agregarPuntos(int misPuntos);
}
