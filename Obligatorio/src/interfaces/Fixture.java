package interfaces;

import dominio.PartidoRusia;
import dominio.SeleccionRusia;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *Fixture
 * Esta clase define el comportamiento que deberia tener un fixture en la
 * aplicacion. La misma debera manejar una estructura con todos los partidos
 * la cual usara para poder actualizar los partidos. Tambien debera tener
 * una estructura alterna de SeleccionRusia para poder llevar la cuenta de 
 * los puntos de cada seleccion por cada grupo. Cada grupo debera estar ordenado
 * por orden de puntos.
 * 
 * 
 * 
 * @author Sebas
 */
public interface Fixture {
    
    
    /**
     * Metodo que permite actualizar resultados al fixture, asigna los puntos
     * correspondientes a cada seleccion, y establece los campos ganador,
     * empate y resultado de cada partido que se encuentre en la estructura
     * segun la lista que recibe
     * @param listaResultado recibe una lista de enteros con el numero de partido
     * el resultado del preimer equipo y el resultado del segundo
     * @return boolean de confirmacion
     */
    boolean cargarResultados(ArrayList<Integer> listaResultado);
    
    /**
     * DarPartido es un metodo que dado un numero de partido devuelve ese partido
     * de la lista de partidos
     * 
     * @param NPartido recibe un entero correspondiente a un numero de partido
     * @return un objeto partidoRusia y NULL si el numero de partido esta fuera
     * del rango de la lista de partidos
     */
    PartidoRusia darPartido(int noPartido);
    
    /**
     * darProximoPartido es un metodo que recibe una fecha y devuelve el
     * numero de partido del proximo partido a esa fecha
     * La razon de este metodo es permitir la implementacion
     * de estaActualizado; metodo de la clase sistema.
     * si dadas dos fechas el proximo partido de ambas es el mismo
     * el sistema estara "up To Date"
     * @param fecha recibe una fecha. 
     * @return un int que equivale al numero de partido del proximo partido
     */
    int darProximoPartido(LocalDateTime fecha);
    
    /**
     * esVacio es un procedimiento que perite a quien lo llama saber si el 
     * fixture fue o no cargado con resultados iniciales. 
     * @return boolean de confirmacion
     */
    boolean esPosicionVacia(int index);
    
    /**
     * darGrupos es un metodo que permite acceder a todos los grupos. 
     * @return un array de arrays con los grupos. La posicion 0 equivale al grupo
     * A
     */
    SeleccionRusia[][] darGrupos();
    
   
}
