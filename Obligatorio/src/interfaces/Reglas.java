package interfaces;

import dominio.FixtureRusia;
import dominio.PartidoRusia;

/**
 * Reglas es una interfaz que permite definir el comportamiento de las reglas
 * parametrizables por la cual los puntos seran asignados a los participantes
 * Reglas sera una atributo del sistema y contendra la informacion siguiente en
 * forma de atributos acorde a lo que establezca el usuario
 * <p>
 * Un multiplicador para cada face, por defecto el multiplicador debera ser uno
 * de esta manera el usuario podra determinar que los puntos valen diferente 
 * dependiendo de la fase que se haga el acierto; de modo logico se entiende que
 * deberia ser mejor puntuado acertar el resultado de una final que de un partido
 * de grupos...
 * <p>
 * Un valor de puntos por acertar los goles, no bastando con solo 
 * acertar al ganador el participante podra agregar puntos adicionales por 
 * acertar el resultado correcto, y la mitad si solo acierta a un equipo
 * <p>
 * Un valor de puntos por acertar el ganador del partido
 * <p> 
 * Un valor de puntos por acertar un partido empatado
 * 
 * @author Sebas
 */

public interface Reglas {

/**
 * darPuntaje es un metodo que calcula el puntaje que se obtiene al comparar 
 * una lsita de apuestas con los resultados en un fixture. El calculo se realiza
 * en funcion de los parametros que el participante alla adjudicado y que fueron 
 * guardados como atributos de esta clase.
 * <p>
 * Los aciertos pueden ser por cantidad de goles o por acertar el ganador.El 
 * participante puede determinar no utilizar la cantidad de goles para puntear
 * y por tanto el modificador sera 0.
 * <p>
 * Los puntos se calculan de la siguiente manera, si el participante acierta al 
 * ganador recibe los puntos por acertar al ganador, recordar que el valor de este 
 * lo determina el usuario y es un atributo de la clase.
 * Luego se le adicciona si corresponde los puntos obetnidos por el acierto a 
 * los goles y finalmente se multiplica por el multiplicador de fase.
 * <p>
 * ejemplo:
 * <p>
 * Entrada: <p>
 * Apuesta[1]: 2 Uruguay Egipto [2,0]<p> 
 * Fixture[2]: 2 Uruguay Egipto [2,0]<p>
 * <p>
 * Suponiendo multiplicador de fases 2 ganador 3 goles 5<p>
 * Salida: 16
 * <p>
 * Entrada: <p>
 * Apuesta[1]:2 Uruguay Rusia [3,0] <p>
 * Fixture[2]:2 Uruguay Rusia [1,0] <p>
 * <p>
 * Suponiendo multiplicador de fases 6 ganador 3 goles 5<p>
 * Salida: 12
 * @param apuestas recibe un partidoRusia como apuesta, si es null retorna 0  
 * <p>
 * 
 * @param fixture recibe un fixtureRusia el cual no puede ser vacio
 * @return un entero equivalente a los puntos obtenidos por el acierto. Si no hay acierto
 * retorna 0
 */    
    int darPuntaje(PartidoRusia apuesta, FixtureRusia fixture);
}
