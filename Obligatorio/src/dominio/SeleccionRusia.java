package dominio;

/**
 * SeleccionRusia es una clase que permite guardar un String con el nombre de la 
 * seleccion y un int con la cantidad de puntos que iene
 * @author Sebas
 */
public class SeleccionRusia {
    
    private String seleccion;
    private int puntos;

    public SeleccionRusia(String seleccion) {
        this.seleccion = seleccion;
        puntos = 0;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public String getSeleccion() {
        return seleccion;
    }

    public int getPuntos() {
        return puntos;
    }
    
    public SeleccionRusia clon() {
        SeleccionRusia seleccion = new SeleccionRusia(new String(this.getSeleccion()));
        seleccion.setPuntos(puntos);   
        return seleccion;
    }
    
   
    
}
