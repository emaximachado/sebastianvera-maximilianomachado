
package dominio;

import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sebas
 */
public class PartidoRusiaTest {
    
    public PartidoRusiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals1() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(2, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals2() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Brasil", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals3() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Rusia", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals4() {
        int[] resultado = {1, 3};
        int[] resultado2 = {2, 4};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado2, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals5() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioB", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals6() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now().minusDays(1), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
    
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals7() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        p1.setGrupo("A");
        p2.setGrupo("B");
        
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals8() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Argentina", true);
        
        p1.setGrupo("A");
        p2.setGrupo("A");
        assertNotEquals(p2, p1);
    }
    /**
     * Test of equals method, of class PartidoRusia.
     */
    @Test
    public void testEquals9() {
        int[] resultado = {1, 3};
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", true);
        PartidoRusia p2 = new PartidoRusia(1, "Uruguay", "Argentina", resultado, "EstadioA", LocalDateTime.now(), true, "Uruguay", false);
        p1.setGrupo("A");
        p2.setGrupo("A");
        
        assertNotEquals(p2, p1);
    }
}
