package dominio;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sebas
 */
public class ParticipanteRusiaTest {
    
    public ParticipanteRusiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Prueba ejecutar el metodo completamente
     */
    @Test
    public void testAgregarApuesta() {
       
        PartidoRusia unPartido = new PartidoRusia(4, "Uruguay", "Rusia", "EstadioA");
        int[] resultado = {1, 0};
        unPartido.setResultado(resultado);
        unPartido.setGrupo("B");
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        listaPartidos[4] = unPartido;
        SeleccionRusia[][] grupos = new SeleccionRusia[7][2];
        grupos[1][0] = new SeleccionRusia("Uruguay");
        grupos[1][1] = new SeleccionRusia("Rusia");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        fixture.setFaseGrupos(grupos);
        ParticipanteRusia unParticipante = new ParticipanteRusia("Juan",fixture);
        
        assertTrue(unParticipante.agregarApuesta(unPartido));
        
    }
    
    
    /**
     * Prueba que determina si el metodo agrega efectivamente la apuesta
     * comparandola con su contra metodo que es devolver las apuestas
     * 
     * se prueba solo una apuesta.
     */
    @Test
    public void testAgregarApuesta2() {
       
        PartidoRusia unPartido = new PartidoRusia(4, "Uruguay", "Rusia", "EstadioA");
        int[] resultado = {1, 0};
        unPartido.setResultado(resultado);
        unPartido.setGrupo("B");
        unPartido.setFechaYHora(LocalDateTime.now());
        unPartido.setEmpate(false);
        unPartido.setGanador("Uruguay");
       
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        listaPartidos[4] = unPartido;
        SeleccionRusia[][] grupos = new SeleccionRusia[7][2];
        grupos[1][0] = new SeleccionRusia("Uruguay");
        grupos[1][1] = new SeleccionRusia("Rusia");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        fixture.setFaseGrupos(grupos);
        ParticipanteRusia unParticipante = new ParticipanteRusia("Juan",fixture);
        unParticipante.agregarApuesta(unPartido);
        
        assertEquals(unPartido, unParticipante.darApuestas().get(0));
        
    }
    
    /**
     * Prueba que determina si el metodo agrega efectivamente la apuesta
     * comparandola con su contra metodo que es devolver las apuestas
     * 
     * se prueban 2 apuestas, 1 con resultados (apuesta activa), y otra sin resultados
     * una apuesta sin realizar
     */
    @Test
    public void testAgregarApuesta3() {
       
        PartidoRusia unPartido = new PartidoRusia(4, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia otroPartido = new PartidoRusia(5, "Brasil", "Argentina", "EstadioB");
        int[] resultado = {1, 0};
        unPartido.setResultado(resultado);
        unPartido.setGrupo("B");
        
        unPartido.setFechaYHora(LocalDateTime.now());
        unPartido.setEmpate(false);
        unPartido.setGanador("Uruguay");
       
        otroPartido.setGrupo("C");
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        listaPartidos[4] = unPartido;
        listaPartidos[5] = otroPartido;
        SeleccionRusia[][] grupos = new SeleccionRusia[7][2];
        grupos[1][0] = new SeleccionRusia("Uruguay");
        grupos[1][1] = new SeleccionRusia("Rusia");
        grupos[2][0] = new SeleccionRusia("Brasil");
        grupos[2][1] = new SeleccionRusia("Argentina");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        fixture.setFaseGrupos(grupos);
        ParticipanteRusia unParticipante = new ParticipanteRusia("Juan",fixture);
        unParticipante.agregarApuesta(unPartido);
        
        assertEquals(unPartido, unParticipante.darApuestas().get(0));
        
    }
    
    
    /**
     * Prueba que determina si el metodo agrega efectivamente la apuesta
     * comparandola con su contra metodo que es devolver las apuestas
     * <p>
     * Se prueba indirectamente darApuestas
     * 
     * se prueban 2 apuestas, 1 con resultados (apuesta activa), y otra sin resultados
     * una apuesta sin realizar, se pregunta por la apuesta sin realizar de modo
     * de probar que efectivamente no este alli.
     */
    @Test (expected = IndexOutOfBoundsException.class)
    public void testAgregarApuesta4() {
       
        PartidoRusia unPartido = new PartidoRusia(4, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia otroPartido = new PartidoRusia(5, "Brasil", "Argentina", "EstadioB");
        int[] resultado = {1, 0};
        unPartido.setResultado(resultado);
        unPartido.setGrupo("B");
       
        otroPartido.setGrupo("C");
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        listaPartidos[4] = unPartido;
        listaPartidos[5] = otroPartido;
        SeleccionRusia[][] grupos = new SeleccionRusia[7][2];
        grupos[1][0] = new SeleccionRusia("Uruguay");
        grupos[1][1] = new SeleccionRusia("Rusia");
        grupos[2][0] = new SeleccionRusia("Brasil");
        grupos[2][1] = new SeleccionRusia("Argentina");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        fixture.setFaseGrupos(grupos);
        ParticipanteRusia unParticipante = new ParticipanteRusia("Juan", fixture);
        unParticipante.agregarApuesta(unPartido);
        
        assertNotEquals(unPartido, unParticipante.darApuestas().get(1));
        
    }

    /**
     * Prueba de vaciar apuestas. Se carga una apuesta y se la elimina
     * se pide las apuestas para comprobar que no esta
     */
    @Test 
    public void testVaciarApuestasParcial() throws IOException {
        
        
        Sistema miSistema = new Sistema();
        
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante("Juan");
        PartidoRusia unPartido = new PartidoRusia(1, "Rusia", "Arabia Saudita", "Luzhniki");
        unPartido.setFechaYHora(LocalDateTime.of(2018, 6, 14, 12, 0));
        unPartido.setEmpate(false);
        unPartido.setGrupo("A");
        unPartido.setGanador("Rusia");
        int[] resultado = {4, 0};
        unPartido.setResultado(resultado);
        ParticipanteRusia juan = miSistema.darParticipante("Juan");
        juan.agregarApuesta(unPartido);
        
        ArrayList<Integer> paraBorrar = new ArrayList<>();
        paraBorrar.add(1);
        juan.vaciarApuestasParcial(paraBorrar);
        
        assertTrue(juan.darApuestas().isEmpty());
    }

}
