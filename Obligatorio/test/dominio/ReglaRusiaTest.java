package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * ReglasRusiaTest, dado que la fecha de los partidos no interfiere en los 
 * puntos que se otorgan, se adjudica null a ese campo. El numero de partido si
 * es importante porque es quien determina a que fase corresponde el partido
 * <p>
 * Se busca: <p>
 * - probar el comportamiento de la clase cuando recibe apuesta o 
 *   fixture vacio <p>
 * - probar los resultados por cada fase incluso casos de borde <p>
 * - probar los resultados de las apuestas en; ganador, goles,
 *   empate en cada fase
 * @author Sebas
 */

public class ReglaRusiaTest {
    
    public ReglaRusiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Prueba con lista de apuestas vacia.
     */
    @Test 
    public void testDarPuntajeApuestasVacias() {
        
        // se crea la apuesta Vacia
        PartidoRusia p1 = null;
        
        //se crea un nuevo fixture vacio
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fasesw
        int[] fases = {1, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        int expResult = 0;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    /**
     * prueba de Fixture Vacio
     */
    @Test (expected = NullPointerException.class) 
    public void testDarPuntajeFixtureVacio() {
        
        // se crea la apuesta
        int[] resultado = {1, 0}; 
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", resultado, "estadio1", null, true, null, true);
       
        //se crea un nuevo fixture vacio
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {1, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        int expResult = 0;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    /**
     * prueba de apuesta acierto completo en fase 1 borde inferior partido 1
     */
    @Test  
    public void testDarPuntajeCompletoFase1BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(1, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*2 = 6
        
        int expResult = 6;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    
    /**
     * prueba de apuesta acierto completo en fase 1 borde superior partido 1
     */
    @Test  
    public void testDarPuntajeCompletoFase1BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(48, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[48] = new PartidoRusia(48, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*2 = 6
        
        int expResult = 6;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }


    /**
     * prueba de apuesta acierto completo en fase 2 borde inferior partido 49
     */
    @Test  
    public void testDarPuntajeCompletoFase2BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(49, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[49] = new PartidoRusia(49, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*3 = 9
        
        int expResult = 9;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    
    /**
     * prueba de apuesta acierto completo en fase 2 borde superior partido 56
     */
    @Test  
    public void testDarPuntajeCompletoFase2BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(56, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[56] = new PartidoRusia(56, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*3 = 9
        
        int expResult = 9;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    
    /**
     * prueba de apuesta acierto completo en fase  borde inferior partido 57
     */
    @Test  
    public void testDarPuntajeCompletoFase3BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(57, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[57] = new PartidoRusia(57, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*4 = 12
        
        int expResult = 12;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
        
    /**
     * prueba de apuesta acierto completo en fase 3 borde superior partido 60
     */
    @Test  
    public void testDarPuntajeCompletoFase3BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(60, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[60] = new PartidoRusia(60, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*4 = 12
        
        int expResult = 12;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    
    /**
     * prueba de apuesta acierto completo en fase 4 borde inferior partido 61
     */
    @Test  
    public void testDarPuntajeCompletoFase4BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(61, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[61] = new PartidoRusia(61, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*5 = 15
        
        int expResult = 15;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
        
    /**
     * prueba de apuesta acierto completo en fase 4 borde superior partido 63
     */
    @Test  
    public void testDarPuntajeCompletoFase4BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(63, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[63] = new PartidoRusia(63, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*5 = 15
        
        int expResult = 15;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    /**
     * prueba de apuesta acierto completo en fase 5 partido 64
     */
    @Test  
    public void testDarPuntajeCompletoFase5() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 0};
        PartidoRusia p1 = new  PartidoRusia(64, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[64] = new PartidoRusia(64, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 6};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*6 = 18
        
        int expResult = 18;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
       
    /**
     * prueba de apuesta acierto completo empate en fase 1 borde inferior partido 1
     */
    @Test  
    public void testDarPuntajeCompletoEmpateFase1BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 1};
        PartidoRusia p1 = new  PartidoRusia(1, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, null, true);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 1};
        
        fixturePartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, null, true);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*2 = 6
        
        int expResult = 6;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    
    /**
     * prueba de apuesta acierto Empate en fase 1 borde superior partido 48
     */
    @Test  
    public void testDarPuntajeCompletoFase1EmpateBordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 1};
        PartidoRusia p1 = new  PartidoRusia(48, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, null, true);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 1};
        
        fixturePartidos[48] = new PartidoRusia(48, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, null, true);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (2 + 1)*2 = 6
        
        int expResult = 6;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

// solo se prueba empate en fase 1, porque las demas fases no admiten empates.
    
    /*
    siguientes pruebas se prueba el acierto de goles parcial, es decir, se aposto
    3-2 y el resultado fue 0-2, no se acierta ganador, pero si los goles de un equipo
    */
     
    
    /**
     * prueba de apuesta acierto parcial en fase 1 borde inferior partido 1
     */
    @Test  
    public void testDarPuntajeParcialFase1BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(1, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta 1 acierto goles, 0 acierto ganador 2 multiplicador (1 + 0)*2 = 2
        
        int expResult = 2;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    
    /**
     * prueba de apuesta acierto Parcial en fase 1 borde superior partido 1
     */
    @Test  
    public void testDarPuntajeParcialFase1BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(48, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[48] = new PartidoRusia(48, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 2, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*2 = 2
        
        int expResult = 2;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }


    /**
     * prueba de apuesta acierto parcial en fase 2 borde inferior partido 49
     */
    @Test  
    public void testDarPuntajeParcialFase2BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(49, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[49] = new PartidoRusia(49, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 3, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*3 = 3
        
        int expResult = 3;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
    
    /**
     * prueba de apuesta acierto parcial en fase 2 borde superior partido 56
     */
    @Test  
    public void testDarPuntajeParcialFase2BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {3, 0};
        PartidoRusia p1 = new  PartidoRusia(56, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Uruguay", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 1};
        
        fixturePartidos[56] = new PartidoRusia(56, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "", true);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta no acierto goles, no acierto ganador 2 multiplicador (0+ 0)*3 = 0
        
        int expResult = 0;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    
    /**
     * prueba de apuesta acierto Parcial en fase  borde inferior partido 57
     */
    @Test  
    public void testDarPuntajeParcialFase3BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(57, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[57] = new PartidoRusia(57, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*4 = 4
        
        int expResult = 4;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
        
    /**
     * prueba de apuesta acierto cparcial en fase 3 borde superior partido 60
     */
    @Test  
    public void testDarPuntajeParcialFase3BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(60, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[60] = new PartidoRusia(60, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 4, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*4 = 4
        
        int expResult = 4;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    
    /**
     * prueba de apuesta acierto parcial en fase 4 borde inferior partido 61
     */
    @Test  
    public void testDarPuntajeParcialFase4BordeInferior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {0, 3};
        PartidoRusia p1 = new  PartidoRusia(61, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[61] = new PartidoRusia(61, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta no acierto goles, 0 acierto ganador 2 multiplicador (1 + 0)*5 = 5
        
        int expResult = 0;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }
    
        
    /**
     * prueba de apuesta acierto parcial en fase 4 borde superior partido 63
     */
    @Test  
    public void testDarPuntajeParcialFase4BordeSuperior() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(63, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[63] = new PartidoRusia(63, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 5};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*5 = 5
        
        int expResult = 5;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    /**
     * prueba de apuesta acierto parcial en fase 5 partido 64
     */
    @Test  
    public void testDarPuntajeParcialFase5() {
        // se crea una apuesta borde inferior 
        int[] resultado = {1, 3};
        PartidoRusia p1 = new  PartidoRusia(64, "Uruguay", "Rusia", resultado, 
                                   "estadio1", null, true, "Rusia", false);
       
        /*
        se crea el fixture como un array de partidos, el indice del array debe 
        de coincidir con el numero de partido
        */ 
        PartidoRusia[] fixturePartidos = new PartidoRusia[65];
        int[] resultadoFixture1 = {1, 0};
        
        fixturePartidos[64] = new PartidoRusia(64, "Uruguay", "Rusia",
                   resultadoFixture1, "estadio1", null, true, "Uruguay", false);
        FixtureRusia fixture = new FixtureRusia(fixturePartidos);
        
        //se crean los multiplicadores de las fases
        int[] fases = {2, 3, 4, 5, 6};
        /*
        se instancia la clase reglasRusia con los multiplicadores de las fases
        y un valor de 2 puntos por acierto de goles 1 por empate y uno por
        ganador
        */
        ReglaRusia instance = new ReglaRusia(fases, 2, 1, 1);
        
        //1er apuesta acierto goles, 1 acierto ganador 2 multiplicador (1 + 0)*6 = 6
        
        int expResult = 6;
        int result = instance.darPuntaje(p1, fixture);
        assertEquals(expResult, result);
    }

    
}
