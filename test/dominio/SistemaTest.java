package dominio;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Sistema implemeta los casos de uso descriptos. Se emplea esta clase para 
 * probar los metodos que permiten la realizacion de dichos casos de uso.
 * @author Sebas
 */
public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test Desactualizado 
    * 
     * Se prueba el metodo creando una instnacia de sistema con una fecha de actualizacion
     * anterior a la actual. Se crea un fixture con dos partidos, uno con una fecha 
     * posterior a la de ultima actualizacion y otro con la fecha de manana.
     * al ejecutarse el metodo debera encontrar que los partdos de las dos fechas difieren
     * por tanto debera retornar false.
     */
    

    /**
     * Prueba que el metodo puede realizarce enteramente y reportar haber llegado 
     * al final
     */
    @Test
    public void testCrearReglas() {
        
        int[] fases = {1, 2, 3, 4, 5};
        int goles = 3;
        int ganador = 2;
        int empate = 2;
        Sistema instance = new Sistema();
        assertTrue(instance.crearReglas(fases, goles, ganador, empate));
        
    }

    /**
     *
     * Prueba que el archivo txt con los partidos es leido con exito
     * se compara el fixture creado al leer el archivo con un instanciado en
     * la clase de prueba. Si la lista de partidos de cada fixture es la misma
     * el archivo fue leidos exitosamente
     */
    @Test
    public void testCrearFixture() throws IOException {
        
        
        String fixtureFile = "TestCargarFixture.txt";
        String resultadoFile = "TestCargarFixtureResultado.txt";
        String grupoFile = "Grupos.txt";
        Sistema miSistema = new Sistema();
        
        miSistema.crearFixture(fixtureFile);
        miSistema.cargarGrupos(grupoFile);
        miSistema.cargarResultados(resultadoFile);
        
        PartidoRusia[] listaPartidosEsperada = new PartidoRusia[64];
        listaPartidosEsperada[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidosEsperada[1].setFechaYHora(LocalDateTime.of(2018, 5, 8, 18, 0));
        listaPartidosEsperada[1].setGrupo("A");
        listaPartidosEsperada[1].setEmpate(true);
        listaPartidosEsperada[1].setGanador("");
        int[] resultado = {1, 1};
        listaPartidosEsperada[1].setResultado(resultado);
        listaPartidosEsperada[2] = new PartidoRusia(2, "Argentina", "Brasil", "EstadioA");
        listaPartidosEsperada[2].setFechaYHora(LocalDateTime.of(2018, 5, 10, 18, 0));
        listaPartidosEsperada[2].setGrupo("B");
        listaPartidosEsperada[2].setEmpate(true);
        listaPartidosEsperada[2].setGanador("");
        listaPartidosEsperada[2].setResultado(resultado);
        
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        for (int i = 0; i < 3; i++) {
            listaPartidos[i] = miSistema.getFixture().darPartido(i);
        }
        
        
        assertArrayEquals(listaPartidosEsperada, listaPartidos);
    }

    /**
     * Prueba la creacion de un participante, comparando el alias ingresado
     * con el alias retornado al pedir al sistema por ese participante.
     * Particularmente se crean 3 participantes para que el sistema tenga que
     * realizar la busqueda y comprobar que retorna el elemento correcto
     * <p>
     * Se prueban indirectamente varios metodos, cargarGrupos ya que es necesario
     * para crear un paricipante y darParticipante ya que se solicita buscar 
     * ese participante por su alias. 
     * <p>
     * Se solicita el primer participante agregado al sistema
     */
    @Test
    public void testCrearParticipanteBordeInferior() throws IOException {
        
        String fileName = "Partidos.txt";
        Sistema miSistema = new Sistema();
        String alias = "Juan";
        String alias2 = "Nicolas";
        String alias3 = "Diego";
        miSistema.crearFixture(fileName);
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante(alias);
        miSistema.crearParticipante(alias2);
        miSistema.crearParticipante(alias3);
        
        assertEquals(miSistema.darParticipante(alias).getAlias(), alias);
    
    }
    
    /**
     * Prueba la creacion de un participante, comparando el alias ingresado
     * con el alias retornado al pedir al sistema por ese participante.
     * Particularmente se crean 3 participantes para que el sistema tenga que
     * realizar la busqueda y comprobar que retorna el elemento correcto
     * <p>
     * Se prueban indirectamente varios metodos, cargarGrupos ya que es necesario
     * para crear un paricipante, darParticipante ya que se solicita buscar 
     * ese participante por su alias, y getAlias para compararlos 
     * <p>
     * Se solicita el ultimo participante agregado al sistema
     */
    @Test
    public void testCrearParticipanteBordeSuperior() throws IOException {
        
        String fileName = "Partidos.txt";
        Sistema miSistema = new Sistema();
        String alias = "Juan";
        String alias2 = "Nicolas";
        String alias3 = "Diego";
        miSistema.crearFixture(fileName);
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante(alias);
        miSistema.crearParticipante(alias2);
        miSistema.crearParticipante(alias3);
        
        assertEquals(miSistema.darParticipante(alias3).getAlias(), alias3);
    
    }

    /**
     * CargarResultados tiene dos instancias, primera poder levantar el archivo de
     * texto y crear el array de enteros correctamente y luego con ese array
     * llama al metodo cargarResultados de fixtureRusia quien actualiza el fixture
     * <p>
     * En esta instancia se prueba la carga del archivo ya que en las pruebas unitarias 
     * de FixtureRusia se prueba la actualizacion del fixture.
     * <p>
     * Se prueba el primer partido
     */
    @Test
    public void testCargarResultados1() throws Exception {
        
        String fileName = "Resultados";
        Sistema miSistema = new Sistema();
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.cargarResultados("Resultados.txt");
        FixtureRusia fixture = miSistema.getFixture();
        PartidoRusia partido1 = new PartidoRusia(1, "Rusia", "Arabia Saudita", "Luzhniki");
        partido1.setFechaYHora(LocalDateTime.of(2018, 6, 14, 12, 0));
        partido1.setEmpate(false);
        partido1.setGrupo("A");
        partido1.setGanador("Rusia");
        int[] resultado = {5, 0};
        partido1.setResultado(resultado);
        
        assertEquals(partido1, fixture.darPartido(1));
        
    }

    /**
     * Test of asignarPuntos method, of class Sistema.
     */
    @Test
    public void testAsignarPuntos() throws IOException {
        
        Sistema miSistema = new Sistema();
        int[] fases = {1, 2, 3, 4, 5};
        miSistema.crearReglas(fases, 3, 2, 2);
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante("Juan");
        miSistema.cargarResultados("Resultados.txt");
        
        PartidoRusia partido1 = new PartidoRusia(1, "Rusia", "Arabia Saudita", "Luzhniki");
        partido1.setFechaYHora(LocalDateTime.of(2018, 6, 14, 12, 0));
        partido1.setEmpate(false);
        partido1.setGrupo("A");
        partido1.setGanador("Rusia");
        int[] resultado = {5, 0};
        partido1.setResultado(resultado);
        
        ParticipanteRusia participante = miSistema.darParticipante("Juan");
        
        participante.agregarApuesta(partido1);
                
        miSistema.asignarPuntos();
        
        assertEquals(5, participante.getPuntos());
                
        
    }
    
    /**
     * Test of asignarPuntos method, of class Sistema.
     */
    @Test
    public void testAsignarPuntosApuestasVacias() throws IOException {
        
        Sistema miSistema = new Sistema();
        int[] fases = {1, 2, 3, 4, 5};
        miSistema.crearReglas(fases, 3, 2, 2);
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante("Juan");
        miSistema.cargarResultados("Resultados.txt");
        
        ParticipanteRusia participante = miSistema.darParticipante("Juan");
        miSistema.asignarPuntos();
        
        assertEquals(0, participante.getPuntos());
                
        
    }
    
    
    /**
     * Test of asignarPuntos method, of class Sistema.
     */
    @Test
    public void testAsignarPuntosConApuestasDePartidosNoJugados() throws IOException {
    
            
        Sistema miSistema = new Sistema();
        int[] fases = {1, 2, 3, 4, 5};
        miSistema.crearReglas(fases, 3, 2, 2);
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante("Juan");
        miSistema.cargarResultados("Resultados.txt");
        
        PartidoRusia partido1 = new PartidoRusia(1, "Rusia", "Arabia Saudita", "Luzhniki");
        partido1.setFechaYHora(LocalDateTime.of(2018, 6, 14, 12, 0));
        partido1.setEmpate(false);
        partido1.setGrupo("A");
        partido1.setGanador("Rusia");
        int[] resultado = {5, 0};
        partido1.setResultado(resultado);
    
        PartidoRusia partido2 = new PartidoRusia(64, "Uruguay", "Alemania", "Luzhniki");
        partido2.setFechaYHora(LocalDateTime.of(2018, 7, 15, 12, 0));
        partido2.setEmpate(false);
        partido2.setGrupo("A");
        partido2.setGanador("Uruguay");
        int[] resultado2 = {5, 0};
        partido2.setResultado(resultado2);
    
        
        ParticipanteRusia participante = miSistema.darParticipante("Juan");
        
        participante.agregarApuesta(partido1);
                
        miSistema.asignarPuntos();
        
        assertEquals(5, participante.getPuntos());
    
    
    
    }
    
    /**
     * Test of asignarPuntos method, of class Sistema.
     */
    @Test
    public void testAsignarPuntosDespuesDeLimpiarApuestas() throws IOException {
        
        Sistema miSistema = new Sistema();
        int[] fases = {1, 2, 3, 4, 5};
        miSistema.crearReglas(fases, 3, 2, 2);
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.crearParticipante("Juan");
        miSistema.cargarResultados("Resultados.txt");
        
        PartidoRusia partido1 = new PartidoRusia(1, "Rusia", "Arabia Saudita", "Luzhniki");
        partido1.setFechaYHora(LocalDateTime.of(2018, 6, 14, 12, 0));
        partido1.setEmpate(false);
        partido1.setGrupo("A");
        partido1.setGanador("Rusia");
        int[] resultado = {5, 0};
        partido1.setResultado(resultado);
        
        ParticipanteRusia participante = miSistema.darParticipante("Juan");
        participante.agregarApuesta(partido1);
        miSistema.asignarPuntos();
        
        
        PartidoRusia partido2 = new PartidoRusia(64, "Uruguay", "Alemania", "Luzhniki");
        partido2.setFechaYHora(LocalDateTime.of(2018, 7, 15, 12, 0));
        partido2.setEmpate(false);
        partido2.setGrupo("A");
        partido2.setGanador("Uruguay");
        int[] resultado2 = {5, 0};
        partido2.setResultado(resultado2);
        
        assertEquals(5, participante.getPuntos());
                
        
    }
  
    @Test
    public void testEstaActualizadoFalse() throws IOException {
        
        
        Sistema miSistema = new Sistema();
        miSistema.crearFixture("Partidos.txt");
        assertFalse(miSistema.estaActualizado());
        
    }
    
    @Test
    public void testEstaActualizadoTrue() throws IOException {
        
        
        Sistema miSistema = new Sistema();
        miSistema.crearFixture("Partidos.txt");
        miSistema.cargarGrupos("Grupos.txt");
        miSistema.cargarResultados("Resultados.txt");
        assertTrue(miSistema.estaActualizado());
        
    }
  
}
