package dominio;

import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sebas
 */
public class FixtureRusiaTest {
    
    public FixtureRusiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of darPartido method, of class FixtureRusia.
     */
    @Test
    public void testDarPartido() {
        
        int noPartido = 3;
        
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[3] = p1;
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        
        int expResult = p1.getNumeroPartido();
        int result = fixture.darPartido(noPartido).getNumeroPartido();
        assertEquals(expResult, result);
    }

    /**
     * Test of darProximoPartido method, of class FixtureRusia.
     */
    @Test
    public void testDarProximoPartido() {
        
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia p2 = new PartidoRusia(2, "Uruguay", "Rusia", "EstadioA");
        p1.setFechaYHora(LocalDateTime.now().plusDays(1));
        p2.setFechaYHora(LocalDateTime.now().plusDays(2));
        listaPartidos[1] = p1;
        listaPartidos[2] = p2;
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        LocalDateTime fecha = LocalDateTime.now();
        int expResult = 1;
        int result = fixture.darProximoPartido(fecha);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of darProximoPartido method, of class FixtureRusia.
     */
    @Test
    public void testDarProximoPartidoNoHayProximo() {
        
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia p2 = new PartidoRusia(2, "Uruguay", "Rusia", "EstadioA");
        p1.setFechaYHora(LocalDateTime.now().plusDays(1));
        p2.setFechaYHora(LocalDateTime.now().plusDays(2));
        listaPartidos[1] = p1;
        listaPartidos[2] = p2;
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        LocalDateTime fecha = LocalDateTime.now().plusDays(3);
        int expResult = 0;
        int result = fixture.darProximoPartido(fecha);
        assertEquals(expResult, result);
    }


    /**
     * Test of esPosicionVacia method, of class FixtureRusia.
     */
    @Test
    public void testEsPosicionVacia() {
        
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia p2 = new PartidoRusia(2, "Uruguay", "Rusia", "EstadioA");
        p1.setFechaYHora(LocalDateTime.now().plusDays(1));
        p2.setFechaYHora(LocalDateTime.now().plusDays(2));
        listaPartidos[1] = p1;
        listaPartidos[2] = p2;
        int index = 3;
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        assertTrue(fixture.esPosicionVacia(index));
    }
    @Test
    public void testEsPosicionVaciaFalse() {
        
        PartidoRusia[] listaPartidos = new PartidoRusia[64];
        PartidoRusia p1 = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        PartidoRusia p2 = new PartidoRusia(2, "Uruguay", "Rusia", "EstadioA");
        p1.setFechaYHora(LocalDateTime.now().plusDays(1));
        p2.setFechaYHora(LocalDateTime.now().plusDays(2));
        listaPartidos[1] = p1;
        listaPartidos[2] = p2;
        int index = 1;
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        assertFalse(fixture.esPosicionVacia(index));
    }

    /**
     * Test of darGrupos method, of class FixtureRusia.
     */
    @Test
    public void testDarGrupos() {
        
        FixtureRusia fixture = new FixtureRusia(null);
        SeleccionRusia[][] grupos = new SeleccionRusia[1][1];
        
        grupos[0][0] = new SeleccionRusia("Uruguay");
        fixture.setFaseGrupos(grupos);
        
        SeleccionRusia[][] expResult = grupos;
        SeleccionRusia[][] result = fixture.darGrupos();
        assertArrayEquals(expResult, result);
        
    }

    /**
     * Prueba de Resultado empate, prueba puntaje en grupos
     */
    @Test
    public void testCargarResultadosEmpateGrupos() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(2);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
        
        SeleccionRusia equipoFixture1 = fixture.darGrupos()[0][0];
        SeleccionRusia equipoFixture2 = fixture.darGrupos()[0][1];
        int puntosEmpate = 1;
        assertEquals(puntosEmpate, equipoFixture1.getPuntos());
        assertEquals(puntosEmpate, equipoFixture2.getPuntos());
    }
    
    /**
     * Prueba de Resultado empate listaPartidos, campo ganador 
     */
    @Test
    public void testCargarResultadosEmpatePartidos() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(2);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        String ganador = "";
        
        assertEquals(ganador, partidoResultado.getGanador());
    }
    
    /**
     * Prueba de Resultado empate listaPartidos, campo empate 
     */
    @Test
    public void testCargarResultadosEmpatePartidos2() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(2);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        boolean empate = true;
        
        assertEquals(empate, partidoResultado.isEmpate());
    }
    
    /**
     * Prueba de Resultado empate listaPartidos, campo resultado
     */
    @Test
    public void testCargarResultadosEmpatePartidos3() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(2);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
        int[] resultadoEsperado = {2, 2};
        PartidoRusia partidoResultado = fixture.darPartido(1);
        
        
        assertArrayEquals(resultadoEsperado, partidoResultado.getResultado());
    }
    
    /**
     * Prueba de Resultado con un ganador, prueba puntaje en grupos
     */
    @Test
    public void testCargarResultadosGanadorGrupos() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(4);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
        
        SeleccionRusia equipoFixture1 = fixture.darGrupos()[0][0];
        
        int puntosGanador = 3;
        assertEquals(puntosGanador, equipoFixture1.getPuntos());
        
    }
    
    /**
     * Prueba de Resultado con un ganador listaPartidos, campo ganador 
     */
    @Test
    public void testCargarResultadosGanadorPartidos() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 3 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(3);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        String ganador = "Uruguay";
        
        assertEquals(ganador, partidoResultado.getGanador());
    }
    
    /**
     * Prueba de Resultado con un ganador listaPartidos, campo empate 
     */
    @Test
    public void testCargarResultadosGanandorPartidos2() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 5 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(5);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        boolean empate = false;
        
        assertEquals(empate, partidoResultado.isEmpate());
    }
    
    
    /**
     * Prueba de Resultado con un ganador listaPartidos, campo resultado
     */
    @Test
    public void testCargarResultadosGanandorPartidos3() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 3 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(3);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
        int[] resultadoEsperado = {3, 2};
        PartidoRusia partidoResultado = fixture.darPartido(1);
        
        
        assertArrayEquals(resultadoEsperado, partidoResultado.getResultado());
    }
    
    
    /**
     * Prueba de Resultado con el ganador opuesto, prueba puntaje en grupos
     */
    @Test
    public void testCargarResultadosGanadorGruposB() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 2 a 4...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(2);
        listaResultado.add(4);
        fixture.cargarResultados(listaResultado);
        
        SeleccionRusia equipoFixture2 = fixture.darGrupos()[0][0];
        
        int puntosGanador = 3;
        assertEquals(puntosGanador, equipoFixture2.getPuntos());
        
    }
    
    /**
     * Prueba de Resultado con el ganador opuesto listaPartidos, campo ganador 
     */
    @Test
    public void testCargarResultadosGanadorPartidosB() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 1 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(1);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        String ganador = "Rusia";
        
        assertEquals(ganador, partidoResultado.getGanador());
    }
    
    /**
     * Prueba de Resultado con el ganador opuesto listaPartidos, campo empate 
     */
    @Test
    public void testCargarResultadosGanandorPartidos2B() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 1 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(1);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
   
        PartidoRusia partidoResultado = fixture.darPartido(1);
        boolean empate = false;
        
        assertEquals(empate, partidoResultado.isEmpate());
    }
    
    
    /**
     * Prueba de Resultado con el ganador opuesto listaPartidos, campo resultado
     */
    @Test
    public void testCargarResultadosGanandorPartidos3B() {
        //Se crean los partidos y se instancia fixture
        PartidoRusia[] listaPartidos = new PartidoRusia[3];
        listaPartidos[1] = new PartidoRusia(1, "Uruguay", "Rusia", "EstadioA");
        listaPartidos[1].setGrupo("A");
        FixtureRusia fixture = new FixtureRusia(listaPartidos);
        
        //se crea el grupo y se lo setea a fixture
        SeleccionRusia[][] grupos = new SeleccionRusia[1][2];
        grupos[0][0] = new SeleccionRusia("Uruguay");
        grupos[0][1] = new SeleccionRusia("Rusia");
        
        fixture.setFaseGrupos(grupos);
        
        //partido 1 resultado 1 a 2...
        ArrayList<Integer> listaResultado = new ArrayList<>();
        listaResultado.add(1);
        listaResultado.add(1);
        listaResultado.add(2);
        fixture.cargarResultados(listaResultado);
        int[] resultadoEsperado = {1, 2};
        PartidoRusia partidoResultado = fixture.darPartido(1);
        
        
        assertArrayEquals(resultadoEsperado, partidoResultado.getResultado());
    }
    
    
}
